import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("")
public class Serverlet extends HttpServlet {
   private static final long serialVersionUID = 1L;

   public Serverlet() {
      super();
   }

   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {      
      PrintWriter out = response.getWriter();
      out.println(" <html lang='en'> <title>CSCI 4830</title> <meta charset='UTF-8'> <meta name='viewport' content='width=device-width, initial-scale=1'> <link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'> <link rel='stylesheet' href='https://www.w3schools.com/lib/w3-theme-black.css'> <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'> <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'> <style> .column {\n" + 
      		"  float: left;\n" + 
      		"  width: 50%;\n" + 
      		"}\n" + 
      		"\n" + 
      		"/* Clear floats after the columns */\n" + 
      		".row:after {\n" + 
      		" padding-bottom: 25px" +
      		"  content: \"\";\n" + 
      		"  display: table;\n" + 
      		"  clear: both;\n" + 
      		"} html,body,h1,h2,h3,h4,h5,h6 {font-family: 'Roboto', sans-serif;} .w3-sidebar { z-index: 3; width: 250px; top: 43px; bottom: 0; height: inherit; } </style> <body>  <div class='w3-top'> <div class='w3-bar w3-theme w3-top w3-left-align w3-large'> <a class='w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1' href='javascript:void(0)' onclick='w3_open()'><i class='fa fa-bars'></i></a> <a href='#' class='w3-bar-item w3-button w3-theme-l1'>50/50</a> </div> </div>  <div class='w3-main' style='margin-left:250px'> <div class='w3-row w3-padding-64'> <div class='w3-twothird w3-container'> <h1 class='w3-text-teal'>Would You Rather</h1> <h5> The Plan is simple, select the option that you would rather do! Your answer will be store in the database for later reference! </h5> ");
    	  Connection connection = null;
      PreparedStatement preparedStatement = null;
      try {
         DBConnection.getDBConnection(getServletContext());
         connection = DBConnection.connection;
            String selectSQL = "select * from questions";
            preparedStatement = connection.prepareStatement(selectSQL);
         
         
         ResultSet rs = preparedStatement.executeQuery();
         while (rs.next()) {
            String desc_1 = rs.getString("description_1");
            String pic_1 = rs.getString("picture_1_id");
            String desc_2 = rs.getString("description_2");
            String pic_2 = rs.getString("picture_2_id");
            getServletContext().getRealPath("/WEB-INF/pics/" + pic_1 + ".jpg");
            out.println("<div class='row'>");
            out.println("<div class='column'> " + desc_1 + "</div>");
            out.println("<div class='column'> " + desc_2 + "</div>");
           
            out.println("</div>"); 
            out.println("<div class='row'>");

            out.println("<div class='column'> <img style='height:175px; width:300px;' src='" + getServletContext().getRealPath("/") + "/WEB-INF/pics/" + pic_2 + ".png" + "'> </img></div>");
            out.println("<div class='column'> <img style='height:175px; width:300px;' src='" + getServletContext().getRealPath("/") + "/WEB-INF/pics/" + pic_1 + ".png" + "'> </img></div>");

            out.println("<div class='row'>");
            out.println("<div class='column'> " + "<form action=\"\" method=\"post\"><input type=\"submit\" name=\"button1\" value=\"Vote 1\" /></form> </div>");
            out.println("<div class='column'> " + "<form action=\"\" method=\"post\"><input type=\"submit\" name=\"button2\" value=\"Vote 2\" /></form> </div>");
            out.println("</div>");
         }
         out.println("</div>  <div class='w3-row w3-padding-64'> <div class='w3-twothird w3-container'> </div> </div> </body> <script> function vote1(){ console.log('You voted 1'); } function vote2(){ console.log('You voted 2'); } </script> </html>");
         rs.close();
         preparedStatement.close();
         connection.close();
      } catch (SQLException se) {
         se.printStackTrace();
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            if (preparedStatement != null)
               preparedStatement.close();
         } catch (SQLException se2) {
         }
         try {
            if (connection != null)
               connection.close();
         } catch (SQLException se) {
            se.printStackTrace();
         }
      }
   }
   
   
   @SuppressWarnings("resource")
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      doGet(request, response);
      
      PrintWriter out = response.getWriter();
      String question = "";
      if(request.getParameter("button1") != null)
      {
    	  out.println("<div class='w3-row' w3-padding-64> <div class='w3-twothird w3-container'>  Vote Casted for #1! </div>");
    	  question = "vote_count_1";
      }
      else if(request.getParameter("button2") != null){
    	  out.println("<div class='w3-row' w3-padding-64> <div class='w3-twothird w3-container'>  Vote Casted for #2! </div>");
    	  question = "vote_count_2";
      }
      
      Connection connection = null;
      PreparedStatement preparedStatement = null;
      try {
         DBConnection.getDBConnection(getServletContext());
         connection = DBConnection.connection;
         String updateSQL = "update questions set " + question + " = " + question + " + 1 where id = 2";
         preparedStatement = connection.prepareStatement(updateSQL);
         preparedStatement.executeUpdate();
         String selectSQL = "select * from questions";
         preparedStatement = connection.prepareStatement(selectSQL);
         ResultSet rs = preparedStatement.executeQuery();
         while (rs.next()) {
        	int count_1 = rs.getInt("vote_count_1");
         	int count_2 = rs.getInt("vote_count_2");
         	out.println("Current Stats: ");
         	out.println("</br>Question 1:" + count_1);
         	out.println("Question 2:" + count_2);
         } rs.close();
         preparedStatement.close();
         connection.close();
      } catch (SQLException se) {
         se.printStackTrace();
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            if (preparedStatement != null)
               preparedStatement.close();
         } catch (SQLException se2) {
         }
         try {
            if (connection != null)
               connection.close();
         } catch (SQLException se) {
            se.printStackTrace();
         }
      }
      }
   }
